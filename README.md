**Base Django template**

Just the simplest Django-template you've ever seen (maybe no).

This template consists a set of settings, which allow to configure a project with any static files.

**Run project**

You should activate venv at first:

$~source artvenv/bin/activate 

Then you should run server:

(venv)$~ cd artnet

(venv)artnet$~ manage.py runserver

Now you can open page in browser with URL http://localhost:8000 or another URLs described in urls.py